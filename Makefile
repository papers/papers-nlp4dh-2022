MAIN=paper.tex
PDFS=$(MAIN:.tex=.pdf)
TEXFILE=
BIBFILE=anthology.bib quellen.bib

TEX=xelatex -synctex=1 -interaction=nonstopmode
BIBTEX=bibtex
VIEWER=evince


ifdef VERBOSE
  Q :=
else
  Q := @
endif


#!!!!!DO NOT EDIT ANYTHING UNDER THIS LINE!!!!!
all: $(PDFS)
	make view

%.pdf: %.tex $(TEXFILE) %.aux %.bbl
	$(Q)$(TEX) $<
	$(Q)$(TEX) $<

%.bbl: %.aux $(BIBFILE)
	$(Q)$(BIBTEX) $<

%.aux: %.tex $(TEXFILE)
	$(Q)$(TEX) $<

view: $(PDFS)
	$(Q)$(VIEWER) $^ &

clean:
	$(Q)rm -f *.aux *.bbl *.blg *.lot *.lof *.log *.nlo *.toc *.out *.ind *.ilg *.equ *.idx *.lol *.nav *.snm *.gnuplot *.fls *.fdb_latexmk *.bcf *.run.xml *.synctex.gz *-blx.bib tikz/*.aux
